terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~>3.27"
      }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
    profile = "default"
    region = var.region
}

resource "aws_instance" "terraform_deployment" {
    ami = var.ami_type
    instance_type = var.instance_type

    tags = {
        Name = var.name
    }
}
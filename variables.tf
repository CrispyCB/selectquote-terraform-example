variable region {
  type        = string
  default     = "us-east-1"
  description = "The region to deploy all AWS infrastructure to."
}

variable ami_type {
  type        = string
  default     = "ami-0e1d30f2c40c4c701"
  description = "The AMI to use in EC2 instance deployment."
}

variable instance_type {
    type = string
    default = "t2.micro"
    description = "The instance type/size to be used in this specific deployment."
}


variable name {
  type        = string
  default     = "ExampleTerraformEC2Deployment"
  description = "Name of the instance being deployed to EC2."
}

